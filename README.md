# Little Sun Gazer Python
### A small python script that displays the relative positions of the Sun and Moon.

---

I saw a [post in r/space](https://www.reddit.com/r/space/comments/tvbwit/ive_built_this_battery_powered_sun_tracking/) to the physical [Little Sun Gazer](https://github.com/dr-mod/little-sun-gazer) project and decided to make a software version.

![pic](docs/pic.jpg)

If you want to build one of your own, go for it!  If you want it running in a window, give this one a try.

## Usage
* Install [Python](https://www.python.org/downloads/).
* [Download](https://bitbucket.org/jzielke/little-sun-gazer-python/get/HEAD.zip) or clone this repository.
* Edit gazer.ini with your latitude and longitude.
* Run \__main__.py

## Build
This project is setup to run as a Python Zip Application.  To build it run:
```sh
zip gazer.pyz *.py requirements.txt
```
Place gazer.pyz and gazer.ini wherever you want.

## Binaries
The [bin](bin/) folder has pre-built binaries.

## Configuration
The only lines you must have in [gazer.ini](gazer.ini) are latitude and longitude.  These are in decimal degrees.  Two decimals are adequate.
```
latitude = 35.38
longitude = -84.69
```
There are an additional 3 optional configuration options.  They work independently of each other.  This will not be an exhaustive list of every combination, but an example of each option.

**paths =** *__sun__, moon, both*  
Which altitude(s) will be charted.

**times =** *__sun__, moon*  
Display sunrise/sunset, or moonrise/moonset times.

**graph =** *__time__, bearing*  
The graph can be midnight-midnight (time) or 0-360 degrees (bearing).  Bearing is very useful if you select *paths = both*.  With *paths = both* and *graph = time* it looks like the Moon eclipses the Sun every day.  
The gap in the Moon's path when *graph = bearing* is due to the Moon not completing an entire circuit of the sky in 24 hours.  The Moon will start on one end of the gap at midnight and make it to the other end of the gap in 24 hours.

| ![sun](docs/sun.png) | ![moon](docs/moon.png) | ![both](docs/both.png) |
| -- | -- | -- |
| paths = sun | paths = moon | paths = both |
| times = sun | times = moon | times = sun |
| graph = time | graph = time | graph = time |

| ![time](docs/both.png) | ![sun](docs/bearing-sun.png) | ![moon](docs/bearing-moon.png) | 
| -- | -- | -- |
| paths = both | paths = both | paths = both |
| times = sun | times = sun | times = moon |
| graph = time | graph = bearing | graph = bearing |

## Links
[Little Sun Gazer](https://github.com/dr-mod/little-sun-gazer) - The original.

[/r/space](https://www.reddit.com/r/space/): news, articles and discussion.

[Computing planetary positions - a tutorial with worked examples](https://www.stjarnhimlen.se/comp/tutorial.html) - This has the equations, explanations, and examples so you can test your code.  Almost everything came from here.

[weewx/Sun.py](https://github.com/weewx/weewx/blob/master/bin/weeutil/Sun.py) - Simple solar noon equation, rev180, and local sidereal time came from here.

[minopret / sunpos](https://github.com/minopret/sunpos) - degrees.py started from the one here.

[Solar elevation angle (for a day) Calculator - High accuracy calculation](https://keisan.casio.com/exec/system/1224682277) - Used to verify sun elevation chart.

[The Planets Today : A live view of the solar system](https://www.theplanetstoday.com/#) - Orbit location was verified here.

[Moonrise and Moonset Calculator](https://www.timeanddate.com/moon/) - Used to verify moon paths, moonrise, and moonset.
