from math import sqrt, hypot, pi, fsum, floor
from degrees import sin, cos, tan, asin, acos, atan, atan2, rev, rev180

def moonpos(d, lat=False, lon=False, stophere='xy'):
    N = rev(125.1228 - 0.0529538083 * d)  # Long ascending node
    i = 5.1454                            # Inclination
    w = rev(318.0634 + 0.1643573223 * d)  # Argument of perigee
    a = 60.2666                           # Mean distance in Earth equatorial radii
    e = 0.054900                          # Eccentricity
    M = rev(115.3654 + 13.0649929509 * d) # Mean anomaly

    # The Moon's eccentricity is large enough to need a second approximation of E.
    # Aproximate the eccentric anomaly
    E0 = rev(M + (180 / pi) * e * sin(M) * (1 + e * cos(M)))
    # Generate a second approximation
    E1 = rev(E0 - (E0 - (180 / pi) * e * sin(E0) - M) / (1 - e * cos(E0)))

    # Calculate rectangular coordinates in the plane of the lunar orbit
    x = a * (cos(E1) - e)
    y = a * (sqrt(1 - e * e)) * sin(E1)

    # Convert to distance and true anomaly
    r = sqrt(x * x + y * y)
    v = rev(atan2(y, x))

    # Convert postion to ecliptic coordinates
    x = r * (cos(N) * cos(v + w) - sin(N) * sin(v + w) * cos(i))
    y = r * (sin(N) * cos(v + w) + cos(N) * sin(v + w) * cos(i))

    if stophere == 'xy':
      return [x, y, r / a]

    # z was not needed before, this is in ecliptic coordinates as above
    z = r * sin(v + w) * sin(i)

    # Longitude, latitude, and distance
    moon_lon = rev(atan2(y, x))
    moon_lat = atan2(z, sqrt(x * x + y * y))
    # r is the same as above
#    r = sqrt(x * x + y * y + z * z)

    # Factor in most important perturbations

    # Fundamental arguments:
    Ls, Ms = sunpos(d, False, False, 'lm')	# Sun's mean longitude and mean anomaly
    # M 					# Moon's mean anomaly
    L = rev(N + w + M)				# Moon's mean longitude
    D = rev(L - Ls)				# Moon's mean elongation
    F = rev(L - N)				# Moon's argument of latitude

    # Perturbations in longitude
    p_lon  = -1.274 * sin(M - 2*D)	# Evection
    p_lon +=  0.658 * sin(2*D)		# Variation
    p_lon += -0.186 * sin(Ms)		# Yearly equation
    p_lon += -0.059 * sin(2*M - 2*D)
    p_lon += -0.057 * sin(M - 2*D + Ms)
    p_lon +=  0.053 * sin(M + 2*D)
    p_lon +=  0.046 * sin(2*D - Ms)
    p_lon +=  0.041 * sin(M - Ms)
    p_lon += -0.035 * sin(D)		# Parallactic equation
    p_lon += -0.031 * sin(M + Ms)
    p_lon += -0.015 * sin(2*F - 2*D)
    p_lon +=  0.011 * sin(M - 4*D)

    # Perturbations in longitude
    p_lat  = -0.173 * sin(F - 2*D)
    p_lat += -0.055 * sin(M - F - 2*D)
    p_lat += -0.046 * sin(M + F - 2*D)
    p_lat +=  0.033 * sin(F + 2*D)
    p_lat +=  0.017 * sin(2*M + F)

    # Perturbations in distance
    p_r  = -0.58 * cos(M - 2*D)
    p_r += -0.46 * cos(2*D)

    # Add perturbations
    moon_lon += p_lon
    moon_lat += p_lat
    r += p_r

    # Obliquity of the ecliptic (same as in Sun calculation)
    oblecl = 23.4393 - 3.563E-7 * d

    # Rectangular ecliptic coordinates
    xe = r * cos(moon_lon) * cos(moon_lat)
    ye = r * sin(moon_lon) * cos(moon_lat)
    ze = r * sin(moon_lat)

    # Rotate to equatorial coordinates
    x = xe
    y = ye * cos(oblecl) - ze * sin(oblecl)
    z = ye * sin(oblecl) + ze * cos(oblecl)

    # Convert to Right Ascension and Declination
    ra = rev(atan2(y, x))
    decl = atan2(z, sqrt(x*x + y*y))

    # Topocentric position

    # Moon's parallax
    mpar = asin(1/r)

    # GMST0 = Sidereal time at Greenwich at 00:00 Universal Time
    gmst0 = (Ls / 15.0 + 12.0) % 24.0

    # Calculate local sidereal time
    hour = (d - int(d)) * 24.0
    sid = gmst0 + hour + lon / 15.0

    # Moon's Hour Angle
    ha = sid * 15.0 - ra
#    ha = sid * 15.04107 - ra

    # Geocentric latitude and distance from Earth's center
    gclat = lat - 0.1924 * sin(2*lat)
    rho = 0.99833 + 0.00167 * cos(2*lat)

    # Auxiliary angle
    g = atan(tan(gclat) / cos(ha))

    # Moon's topocentric position
    ra = ra - mpar * rho * cos(gclat) * sin(ha) / cos(decl)
    decl = decl - mpar * rho * sin(gclat) * sin(g - decl) / sin(g)

    # Convert topocentric position to rectangular coordinates
    x = cos(ha) * cos(decl)
    y = sin(ha) * cos(decl)
    z = sin(decl)

    # Rotate on Y axis (East-West) so Z axis will point to the zenith.
    xhor = x * sin(lat) - z * cos(lat)
    yhor = y
    zhor = x * cos(lat) + z * sin(lat)

    # Calculate azimuth and altitude
    az = atan2(yhor, xhor) + 180.0
    alt = atan2(zhor, sqrt(xhor * xhor + yhor * yhor))

    if (stophere == 'alt'):
        return alt

    if stophere == 'altaz':
        return alt, az

    # The rest started from sunrise/sunset

    # convert gmst0 from hours to degrees
    gmst0 *= 15.0

    # Which moonset?
    # mpar = lunar parallax (calculated above)
    # m_sd = semi-diameter or apparent radius of the Moon's disk in the sky
    #h = -mpar			# Center of Moon's disk touches a mathematical horizon
    #h = -(mpar + m_sd)		# Moon's upper limb touches a mathematical horizon
    #h = -0.583 - mpar		# Center of Moon's disk touches the horizon; atmospheric refraction accounted for
    #h = -0.583 - (mpar+m_sd)	# Moon's upper limb touches the horizon; atmospheric refraction accounted for

    # "-0.583" - mpar degrees
    h = -53.0 / 60.0 - mpar

    # gmst0 is near meridian when moonpos() is called with stophere = 'times'
    # Recalculate sidereal in degrees with 180.0 instead of hour
    sid = rev(gmst0 + 180.0 + lon)
    ut = 12.0 - rev180(sid - ra) / 15.0

    # Calculate how far the Earth must rotate from solar noon to reach h
    lha = (sin(h) - sin(lat) * sin(decl)) / (cos(lat) * cos(decl))

    # Always sunny?
    if lha < -1:
        return -1, -1

    # Always night?
    if lha > 1:
        return -2, -2

    # lha above is really cos(lha), calculate lha and convert from degrees to hours
    hours = acos(lha) / 15.0

    if stophere == 'hours':
        return hours

    # stophere = 'times'
    return ut - hours, ut + hours

def celestial_bodies(d):
    # Calculate Sun's position then reverse it to get the Earth's position.
    xs, ys, rs = sunpos(d)
    earthX = xs * -1.0
    earthY = ys * -1.0

    # Calculate the Moon's position (x, y, r)
    moonxy = moonpos(d)

    return [earthX, earthY, rs] + moonxy

def sunpos(d, lat=False, lon=False, stophere='xy'):
    w = rev(282.9404 + 4.70935E-5 * d)   # Longitude of perihelion
#    a = 1.000000                        # Mean distance, AU)
    e = 0.016709 - 1.151E-9 * d          # Eccentricity
    M = rev(356.0470 + 0.9856002585 * d) # Mean anomaly

    # Sun's mean longitude and mean anomaly for Moon calculations
    if stophere == 'lm':
        return rev(w + M), M

    # The Earth/Sun eccentricity (e) is so small the first approximation of E is sufficient.
    E = rev(M + (180 / pi) * e * sin(M) * (1 + e * cos(M)))

    # If a were not 1.000000 xv and yv would be multiplied by a
    # Sun's rectangular coordinates in the plane of the ecliptic, where the X axis points towards the perihelion
    xv = cos(E) - e
    yv = sin(E) * sqrt(1 - e * e)

    # Convert to distance and true anomaly
    r = sqrt(xv * xv + yv * yv)
    v = atan2(yv, xv)

    # Longitude of the Sun
    lonsun = rev(v + w)

    # Sun's ecliptic rectangular coordinates
    xs = r * cos(lonsun)
    ys = r * sin(lonsun)

    if stophere == 'xy':
        return xs, ys, r

    # Sun's mean longitude
    L = rev(w + M)

    # Obliquity of the ecliptic
    oblecl = 23.4393 - 3.563E-7 * d

    # Rotate to equatorial coordinates
    xe = xs
    ye = ys * cos(oblecl)
    ze = ys * sin(oblecl)

    # Convert to Right Ascension and Declination
    # r is the same as above
#    r = sqrt(xe * xe + ye * ye + ze * ze)
    ra = atan2(ye, xe)
    decl = atan2(ze, sqrt( xe * xe + ye * ye))

    if stophere == 'ra':
        return ra, decl, r

    # GMST0 = Sidereal time at Greenwich at 00:00 Universal Time
    gmst0 = (L / 15.0 + 12.0) % 24.0

    # Calculate local sidereal time
    hour = (d - int(d)) * 24.0
    sid = gmst0 + hour + lon / 15.0

    # Hour Angle
    ha = sid * 15.0 - ra

    # Convert Hour Angle and Declination to rectangular coordinates
    # X axis points to the celestial equator in the south
    # Y axis to the horizon in the west
    # Z axis to the north celestial pole
    # Distance is irrelevant in this calculation, so it is set to 1.0
    x = cos(ha) * cos(decl)
    y = sin(ha) * cos(decl)
    z = sin(decl)

    # Rotate on Y axis (East-West) so Z axis will point to the zenith.
    xhor = x * sin(lat) - z * cos(lat)
    yhor = y
    zhor = x * cos(lat) + z * sin(lat)

    # Calculate azimuth and altitude
    az = atan2(yhor, xhor) + 180.0
    alt = atan2(zhor, sqrt(xhor * xhor + yhor * yhor))

    if stophere == 'altaz':
        return alt, az

    # convert gmst0 from hours to degrees
    gmst0 *= 15.0

    # Which sunset?
    # h = 0.0           # Center of Sun's disk touches a mathematical horizon
    # h = -15.0 / 60.0  # -0.250 Sun's upper limb touches a mathematical horizon
    # h = -35.0 / 60.0  # -0.583 Center of Sun's disk touches the horizon; atmospheric refraction accounted for
    # h = -50.0 / 60.0  # -0.833 Sun's upper limb touches the horizon; atmospheric refraction accounted for
    # h = -6.0          # Civil twilight (one can no longer read outside without artificial illumination)
    # h = -12.0         # Nautical twilight (navigation using a sea horizon no longer possible)
    # h = -15.0         # Amateur astronomical twilight (the sky is dark enough for most astronomical observations)
    # h = -18.0         # Astronomical twilight (the sky is completely dark)

    # "-0.833" degrees for almanac sunrise/sunset time
    h = -50.0 / 60.0

    # gmst0 is at solar noon when sunpos() is called with stophere = 'times'
    # Recalculate sidereal in degrees with 180.0 instead of hour
    sid = rev(gmst0 + 180.0 + lon)
    ut = 12.0 - rev180(sid - ra) / 15.0

    # Calculate how far the Earth must rotate from solar noon to reach h
    lha = (sin(h) - sin(lat) * sin(decl)) / (cos(lat) * cos(decl))

    # Always sunny?
    if lha < -1:
        return -1, -1

    # Always night?
    if lha > 1:
        return -2, -2

    # lha above is really cos(lha), calculate lha and convert from degrees to hours
    hours = acos(lha) / 15.0

    # stophere == 'times'
    return ut - hours, ut + hours
