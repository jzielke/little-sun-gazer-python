import math

def sin(x):
    return math.sin(x * math.pi / 180)

def cos(x):
    return math.cos(x * math.pi / 180)

def tan(x):
    return math.tan(x * math.pi / 180)

def asin(x):
    return (180.0 / math.pi) * math.asin(x)

def acos(x):
    return (180.0 / math.pi) * math.acos(x)

def atan(x):
    return (180.0 / math.pi) * math.atan(x)

def atan2(y, x):
    return (180.0 / math.pi) * math.atan2(y, x)

def rev(x):
    return (x - math.floor(x / 360.0) * 360.0)

def rev180(x):
    return (x - 360.0 * math.floor(x / 360.0 + 0.5))
