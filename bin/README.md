# Little Sun Gazer Python
### Binaries
[gazer.pyz](gazer.pyz) - Python Zip Application.  Use this if you have Python installed.

Created with the following command:
```sh
zip gazer.pyz *.py requirements.txt
```

[gazer.exe](gazer.exe) - Windows Executable - single file.  Created with Python 3.10.4 and PyInstaller 4.10.
```sh
pyinstaller --clean -F -w -n gazer -i gazer.ico __main__.py
```

[gazer.7z](gazer.7z) - Windows Executable - compressed folder.  Unzip folder and run gazer.exe.  Created with Python 3.10.4 and PyInstaller 4.10.
```sh
pyinstaller --clean -w -n gazer -i gazer.ico __main__.py
```

### Configuration
In all cases, you will need [gazer.ini](../gazer.ini) edited with your location information.
