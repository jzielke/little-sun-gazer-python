#!/usr/bin/env python

from sys import version_info
if version_info.major == 2:
    # We are using Python 2.x
    lt_3 = True
    import Tkinter as tk
    from tkFont import Font
elif version_info.major == 3:
    # We are using Python 3.x
    lt_3 = False
    import tkinter as tk
    from tkinter.font import Font

# Optionally update icon
try:
    from PIL import Image, ImageDraw, ImageTk
except ImportError:
    have_pil = False
else:
    have_pil = True

import ctypes
import platform
import configparser

from time import strftime, time, localtime, daylight, timezone, altzone
from solar import *
from degrees import sin, cos, atan2

SEC_IN_DAY = 60 * 60 * 24

def j2000(time):
    # Orbital information used is for Julian epoch J2000.0
    # time() gives seconds since 1 Jan 1970 00:00 UTC = Julian date 2440587.5
    # J2000.0 starts on 0 Jan 2000 00:00 UTC
    # 31 Dec 1999 00:00 UTC = Julian date 2451543.5
    # 2440587.5 - 2451543.5 = -10956.0
    return time / SEC_IN_DAY - 10956.0

def j2local(time, offset):
    time = (time - int(time)) * 24  + 0.00833 - offset / 60 / 60
    while time < 0:
        time += 24
    while time > 24:
        time -= 24
    return time

class Gazer(tk.Tk):

    def __init__(self):
        if platform.system() == 'Windows':
            myappid = u'jzielke.pythonapps.sungazer.1'
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
        tk.Tk.__init__(self)
        self.title('Little Sun Gazer')
        self.geometry('280x480')
#        self.configure(bg='#fff')
        iconstring = b'iVBORw0KGgoAAAANSUhEUgAAANkAAADZAgMAAADQ0H3lAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACVBMVEX///8AAAAOCQXVZ8uRAAAAAWJLR0QCZgt8ZAAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+YFBxIOF1sMOqEAAANBSURBVGje7ZpJstsgEEBFFb3vhXQfssiehXX/q8SDhBh6VGzXT0q9+N9AP3oSCNmapksuueSSf0hgXdebmwprLn892G4JXGA4HHSBdVyQzNgitCQvOwtWT+euDTZsDMhWxnnoMRnso7NGOBN9BoOUOYtBeupZ5eiZaS8MCpqj0dmvzas4yg/LjvKFkjOKvCcil04NiZOiMBZPjklzSr5IMQiDcs54Z+SLiR9FkeO9yTKXvAPKtHJa+DC0NcaNo8Jx/mSNY+JX0sIpaGnhAtE3ZXrNa+nkNFDlwlu5pHK0yklOLwMdiuVmTBVCLwOtc5ZDAxfeyCUDRymd5Czlo4L5NmcpH6X1bQ5NXPiBHBCPRyOXBpX7c1XW1YaO5c6tfu5hjjCocvDkbpraEPDTzdFR1LgXtiYnFzYuOznYuJuT28IbAvwUt2N9YnouGrkoc6Fw+UdzULjbf8KF5towc/AYX93c9sXJ6uWWVztkHxfS1gYfN5f2ZtB2vYRU2uDh4Gjfp7BzWLXx9c+y/h42ynoHmcPWzYPbHLXsL3PNbRm1cLnhXmdMw/759OzgoElo5rmnYminsdwfYstNTWImnsNK+2jr97/UdcQqwMxzoee2ymv398B0aOeJOAS8jSnnF5ajpOJw4PCjXGrc7T72Ev+aCzsH5VoEE7f/P6osHO1x4JajzC6uuopNXDzcLI4mlks9B/UqdXBLvWxYbrw83RweaSmJQY6LX+bmntu3S4XDk1z6LldfgN/g4kluHjhb/fAkl85xoeYc66HZBxxcJBqW9Y4EZ9lfMuW0vp81aXHsn4FsPRyNItcfdac9M2kDmPsDts00fGbuR4lvosD13tfmI+UQGV6rFQQOpXkSEQgd3nDSZrggdsDElW/4yjSkrkGXL8uOZ6YMzexEoiKTzqh03ecl0zK62cWTA8VRnW0fkGkhfwlofAg3VYWejOLo0sBj1RZ7v4hQSHOPAJdylAbH7+FYHerQ8fs7HNzdtv33/vC77Lkwed4vWAr31DG/z1A2TxgDkt6f2Ln9+wLr+xo7d1TR9n7IxlWPUbb3UaiHRFj191+AeCK1yFkurMQj4ge5aj14DZ7CLrnkkksuueQN8gfKrO1LQLaN8wAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAAldEVYdGRhdGU6Y3JlYXRlADIwMjItMDUtMDdUMjI6MTQ6MjMtMDQ6MDCCHr1LAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDIyLTA1LTA3VDIyOjE0OjIzLTA0OjAw80MF9wAAAABJRU5ErkJggg=='
        icon = tk.PhotoImage(data=iconstring)
        self.tk.call('wm', 'iconphoto', self._w, icon)
        self.canvas = tk.Canvas(width=280, height=480, bg='#fff')
        self.canvas.pack(expand=tk.YES, fill=tk.BOTH)
        self.protocol('WM_DELETE_WINDOW', self.real_window_closing)
        self.bind('<Escape>', self.real_window_closing)
        self.bind('q', self.real_window_closing)
        self.clockfont = Font(family='DejaVu Sans Mono', size='48')
        self.font12 = Font(family='DejaVu Sans Mono', size='12')
        self.latitude  = 0
        self.longitude = 0
        self.counter = 0
        self.paths = 'sun'
        self.times = 'sun'
        self.graph = 'time'
        self.utc_offset = 0
        self.dst_today = False
        self.inifile = 'gazer.ini'
        self.readINI()
        self.showGUI()

    def real_window_closing(self, event=None):
        self.destroy()

    def readINI(self):
        config = configparser.ConfigParser()
        try:
            config.read(self.inifile)
        except:
            return
        if config.has_section('Gazer'):
            if config.has_option('Gazer', 'latitude'):
                self.latitude = float(config.get('Gazer', 'latitude').strip())
            if config.has_option('Gazer', 'longitude'):
                self.longitude = float(config.get('Gazer', 'longitude').strip())
            if config.has_option('Gazer', 'paths'):
                self.paths = config.get('Gazer', 'paths').strip()
                if self.paths not in ['sun', 'moon', 'both']:
                    self.paths = 'sun'
            if config.has_option('Gazer', 'times'):
                self.times = config.get('Gazer', 'times').strip()
                if self.times not in ['sun', 'moon']:
                    self.times = 'sun'
            if config.has_option('Gazer', 'graph'):
                self.graph = config.get('Gazer', 'graph').strip()
                if self.graph not in ['time', 'bearing']:
                    self.graph = 'time'

    def sun_paths(self):
        # Define area for sun path chart
        width = 280.0
        height = 120.0
        x = 0
        y = 480 - 127
        n = 60

        # Calculate the times needed
        t = time()
        seconds_offset = t % SEC_IN_DAY
        # For negative timezones...
        # If it is already the next day UTC time, sunrise and sunset times will be calculated for the wrong day.  Subtract 1 day.
        if seconds_offset < self.utc_offset:
            t -= SEC_IN_DAY
        utc_time_day_start = t - seconds_offset
        non_utc_time_day_start = utc_time_day_start + self.utc_offset
        # This is solar noon, not local noon.
        noon = j2000(utc_time_day_start) - (self.longitude / 360.0)
        d = j2000(t)

        angles = []
        mangles = []
        totalmin = totalmax = 0

        # Calculate the sun's altitude at various times
        for i in range(n + 1):
            jd = j2000(non_utc_time_day_start + i * SEC_IN_DAY / n)
            if self.paths == 'sun' or self.paths == 'both':
                angles.append(list(sunpos(jd, self.latitude, self.longitude, 'altaz')))
            if self.paths == 'moon' or self.paths == 'both' or self.times == 'moon':
                mangles.append(list(moonpos(jd, self.latitude, self.longitude, 'altaz')))
        if self.paths == 'sun' or self.paths == 'both':
            totalmax = max(angles)[0]
            totalmin = min(angles)[0]
        if self.paths == 'moon' or self.paths == 'both':
            moonmax = max(mangles)[0]
            if moonmax > totalmax:
                totalmax = moonmax
            moonmin = min(mangles)[0]
            if moonmin < totalmin:
                totalmin = moonmin

        # Scale the altitudes for the screen size
        amplitude = totalmax - totalmin
        for i in range(n + 1):
            if self.paths == 'sun' or self.paths == 'both':
                angles[i][0] += abs(totalmin)
                angles[i][0] /= amplitude
            if self.paths == 'moon' or self.paths == 'both':
                mangles[i][0] += abs(totalmin)
                mangles[i][0] /= amplitude

        # Calculate and display the night/day line
        zero_deg = (1 - abs(totalmin) / amplitude) * height + y
        self.canvas.create_line(0, zero_deg, width, zero_deg)

        # Draw the sun's path
        if self.paths == 'sun' or self.paths == 'both':
            if self.graph == 'time':
                scale = width / n
                for i in range(1, n + 1):
                    x1 = (i - 1) * scale + x
                    x2 = i * scale + x
                    y1 = (1 - angles[i - 1][0]) * height + y
                    y2 = (1 - angles[i][0]) * height + y
                    self.canvas.create_line(x1, y1, x2, y2)
            # self.graph = 'bearing'
            else:
                scale = width / 360.0
                for i in range(1, n + 1):
                    x1 = angles[i - 1][1] * scale + x
                    x2 = angles[i][1] * scale + x
                    y1 = (1 - angles[i - 1][0]) * height + y
                    y2 = (1 - angles[i][0]) * height + y
                    # Catch wrapping around
                    if x1 > x2:
                        self.canvas.create_line(x1, y1, width, y2)
                        x1 = 0
                    self.canvas.create_line(x1, y1, x2, y2)


        # Draw the moon's path
        if self.paths == 'moon' or self.paths == 'both':
            if self.graph == 'time':
                scale = width / n
                for i in range(1, n + 1):
                    x1 = (i - 1) * scale + x
                    x2 = i * scale + x
                    y1 = (1 - mangles[i - 1][0]) * height + y
                    y2 = (1 - mangles[i][0]) * height + y
                    self.canvas.create_line(x1, y1, x2, y2)
            # self.graph = 'bearing'
            else:
                scale = width / 360.0
                for i in range(1, n + 1):
                    x1 = mangles[i - 1][1] * scale + x
                    x2 = mangles[i][1] * scale + x
                    y1 = (1 - mangles[i - 1][0]) * height + y
                    y2 = (1 - mangles[i][0]) * height + y
                    # Catch wrapping around
                    if x1 > x2:
                        self.canvas.create_line(x1, y1, width, y2)
                        x1 = 0
                    self.canvas.create_line(x1, y1, x2, y2)

        # save for drawsun()
        self.pathinfo = [abs(totalmin), amplitude, width, x, height, y]

        # Get sunrise and sunset times in UTC time
        if self.times == 'sun':
            utcr, utcs = sunpos(noon, self.latitude, self.longitude, 'times')

            # If there is no sunrise or sunset utcr and utcs equal each other
            if utcr != utcs:

                # Add 30 seconds (in hours) to round the minutes up and convert to local time
                sunrise = utcr + 0.00833 - self.utc_offset / 60.0 / 60.0
                sunset = utcs + 0.00833 - self.utc_offset / 60.0 / 60.0

                # Calculate sunrise and sunset x positions
                if self.graph == 'time':
                    srx = sunrise * width / 24.0
                    ssx = sunset * width / 24.0
                # self.graph = 'bearing'
                else:
                    # Calculate hours from utcs and utcr
                    hours = (utcs - utcr) / 2
                    # Get Sun bearing at UTC +- hours for sunrise/sunset bearings
                    _, sraz = sunpos(j2000(utc_time_day_start) + 0.5 - hours / 24.0, self.latitude, 0, 'altaz')
                    _, ssaz = sunpos(j2000(utc_time_day_start) + 0.5 + hours / 24.0, self.latitude, 0, 'altaz')
                    srx = sraz * width / 360.0 + x
                    ssx = ssaz * width / 360.0 + x

                # Prevent sunrise and sunset labels from going off screen or colliding
                # Sunrise off the left side?
                if srx < 35:
                    srx2 = 35
                else:
                    srx2 = srx

                # Sunset off the right side?
                if width - ssx < 30:
                    ssx2 = width - 30
                else:
                    ssx2 = ssx

                # Too close together?
                offset = ssx2 - srx2
                if offset < 70:
                    srx2 -= (70 - offset) / 2
                    ssx2 += (70 - offset) / 2

                    # Too far left?
                    if srx2 < 35:
                        srx2 = 35
                        ssx2 = 35 + 70

                    # Too far right?
                    if width - ssx2 < 30:
                        ssx2 = width - 30
                        srx2 = width - 30 - 70

                # Fix times as needed
                while sunrise < 0.0:
                    sunrise += 24.0
                while sunset >= 24.0:
                    sunset -= 24.0

                # Fix the displayed time if DST change happened today
                if self.dst_today:
                    sunrise += self.utc_offset / 60.0 / 60.0
                    sunrise -= self.getdst(utc_time_day_start + sunrise * 60.0 * 60.0) / 60.0 / 60.0
                    sunset += self.utc_offset / 60.0 / 60.0
                    sunset -= self.getdst(utc_time_day_start + sunset * 60.0 * 60.0) / 60.0 / 60.0

                # Put the sunrise time on the graph
                self.canvas.create_line(srx, zero_deg, srx2, y - 3)
                # If the line goes off the screen, continue it on the other side
                if srx < 0:
                    self.canvas.create_line(srx + width, zero_deg, width + 35, y - 3)
                time_str = "{:02d}:{:02d}".format(int(sunrise), int((sunrise - int(sunrise)) * 60.0))
                self.canvas.create_text(srx2, y - 12 - 15, text='sunrise', font=self.font12, fill='#0a0a0a')
                self.canvas.create_text(srx2, y - 12, text=time_str, font=self.font12, fill='#0a0a0a')

                # Put the sunset time on the graph
                self.canvas.create_line(ssx, zero_deg, ssx2, y - 3)
                # If the line goes off the screen, continue it on the other side
                if ssx > width:
                    self.canvas.create_line(ssx - width, zero_deg, -30, y - 3)
                time_str = "{:02d}:{:02d}".format(int(sunset), int((sunset - int(sunset)) * 60.0))
                self.canvas.create_text(ssx2, y - 12 - 15, text='sunset', font=self.font12, fill='#0a0a0a')
                self.canvas.create_text(ssx2, y - 12, text=time_str, font=self.font12, fill='#0a0a0a')

        elif self.times == 'moon':

            # Use graph to get starting point for moon at meridian
            moonpeak = j2000(utc_time_day_start + mangles.index(max(mangles)) * SEC_IN_DAY / n)

            # Iterate through to find the peak
            moonpeak = self.getpeak(moonpeak)

            # If it is the next day UTC time, times will be calculated for the wrong day.
            daystart = int(j2000(non_utc_time_day_start))
            if int(moonpeak) > daystart:
                offset = 24
            else:
                offset = 0

            mtimes = []
            # Current peak
            utcr, utcs = moonpos(moonpeak, self.latitude, self.longitude, 'times')

            # If there is no moonrise or moonset utcr and utcs equal each other
            if utcr != utcs:
                utcr += offset
                utcs += offset
                # Add 30 seconds (in hours) to round the minutes up and convert to local time
                time1 = utcr + 0.00833 - self.utc_offset / 60.0 / 60.0
                time2 = utcs + 0.00833 - self.utc_offset / 60.0 / 60.0

                if time1 > 0:
                    lst = list(self.getchange(moonpeak, 'rise'))
                    lst.insert(1, 'moonrise')
                    mtimes.append(lst)

                    # Check previous day
                    prevpeak = self.getpeak(moonpeak - 1.03372)
                    if int(prevpeak) == daystart:
                        offset = 24
                    else:
                        offset = 0
                    _, prev_utcs = moonpos(prevpeak, self.latitude, self.longitude, 'times')
                    prev_utcs += offset
                    time3 = prev_utcs + 0.00833 - self.utc_offset / 60.0 / 60.0
                    if time3 > 24:
                        time3 -= 24
                        lst = list(self.getchange(prevpeak, 'set'))
                        lst.insert(1, 'moonset')
                        mtimes.append(lst)

                if time2 < 24:
                    lst = list(self.getchange(moonpeak, 'set'))
                    lst.insert(1, 'moonset')
                    mtimes.append(lst)

                    # Check next day
                    nextpeak = self.getpeak(moonpeak + 1.03372)
                    if int(nextpeak) > daystart +1 :
                        offset = 24
                    else:
                        offset = 0
                    next_utcr, _ = moonpos(nextpeak, self.latitude, self.longitude, 'times')
                    next_utcr += offset
                    time4 = next_utcr + 0.00833 - self.utc_offset / 60.0 / 60.0
                    if time4 < 0:
                        time4 += 24
                        lst = list(self.getchange(nextpeak, 'rise'))
                        lst.insert(1, 'moonrise')
                        mtimes.append(lst)

            # Prevent labels from going off screen or colliding
            pairx = []
            for pair in mtimes:
                if self.graph == 'time':
                    x1 = pair[0] * width / 24.0 + x
                # self.graph = 'bearing'
                else:
                    x1 = pair[2] * width / 360.0 + x

                # Too far to the left?
                if x1 < 42:
                    x2 = 42
                # Too far to the right?
                elif width - x1 < 42:
                    x2 = width - 42
                # Otherwise leave it alone for now
                else:
                    x2 = x1
                pairx.append([pair[0], pair[1], x1, x2])

            # For each pair, look for collisions
            for i in range(len(pairx)):
                for j in range(i + 1, len(pairx)):
                    offset = abs(pairx[i][3] - pairx[j][3])
                    # Too close together
                    if offset < 90:
                        # if 'i' is on the left, move it left
                        if pairx[i][3] < pairx[j][3]:
                            pairx[i][3] -= (90 - offset) / 2
                            pairx[j][3] += (90 - offset) / 2
                            # Too far to the left?
                            if pairx[i][3] < 42:
                                pairx[i][3] = 42
                                pairx[j][3] = 42 + 90
                            # Too far to the right?
                            if width - pairx[j][3] < 42:
                                pairx[j][3] = width - 42
                                pairx[i][3] = width - 42 - 90
                        # if 'j' is on the left, move it left
                        else:
                            pairx[i][3] += (90 - offset) / 2
                            pairx[j][3] -= (90 - offset) / 2
                            # Too far to the left?
                            if pairx[j][3] < 42:
                                pairx[j][3] = 42
                                pairx[i][3] = 42 + 90
                            # Too far to the right?
                            if width - pairx[i][3] < 42:
                                pairx[i][3] = width - 42
                                pairx[j][3] = width - 42 - 90

            # Put the moonrise and set times on the graph
            for pair in pairx:
                # Fix the displayed time if DST change happened today
                if self.dst_today:
                    pair[0] += self.utc_offset / 60.0 / 60.0
                    pair[0] -= self.getdst(utc_time_day_start + pair[0] * 60.0 * 60.0) / 60.0 / 60.0
                self.canvas.create_line(pair[2], zero_deg, pair[3], y - 3)
                time_str = "{:02d}:{:02d}".format(int(pair[0]), int((pair[0] - int(pair[0])) * 60.0))
                self.canvas.create_text(pair[3], y - 12 - 15, text=pair[1], font=self.font12, fill='#0a0a0a')
                self.canvas.create_text(pair[3], y - 12, text=time_str, font=self.font12, fill='#0a0a0a')

        # Since the sun is redrawn every 4 minutes, use a function
        self.drawsun()

    def getpeak(self, moonpeak):
        # Iterate to find the peak moon position.  Graph is +- 24 minutes

        # Back up 144 min - needed when the meridian happened the day before
        moonpeak -= 0.1

        # Coarse
        alt = moonpos(moonpeak, self.latitude, self.longitude, 'alt')
        oldalt = -100
        i = 0
        while alt > oldalt and i < 1000:
            oldalt = alt
            moonpeak += 0.02
            alt = moonpos(moonpeak, self.latitude, self.longitude, 'alt')
            i += 1
        moonpeak -= 0.04

        # Medium
        alt = moonpos(moonpeak, self.latitude, self.longitude, 'alt')
        oldalt = -100
        i = 0
        while alt > oldalt and i < 1000:
            oldalt = alt
            moonpeak += 0.001
            alt = moonpos(moonpeak, self.latitude, self.longitude, 'alt')
            i += 1
        moonpeak -= 0.002

        # Fine
        alt = moonpos(moonpeak, self.latitude, self.longitude, 'alt')
        oldalt = -100
        i = 0
        while alt > oldalt and i < 1000:
            oldalt = alt
            moonpeak += 0.0001
            alt = moonpos(moonpeak, self.latitude, self.longitude, 'alt')
            i += 1
        moonpeak -= 0.0001

        return moonpeak

    def getchange(self, moonpeak, riseset):

        # Find (+ to -) or (- to +) changes

        # If we are looking for a moonrise, we start positive and back up until we are negative
        if riseset == 'rise':
            start = 1
        # The other option is 'set', start negative and back up until positive
        else:
            start = -1

        # Get a coarse starting point
        hours = moonpos(moonpeak, self.latitude, self.longitude, 'hours')

        # Subtract the hours for rise, add the hours for set
        hours = hours * start * -1
        tryme = moonpeak + hours / 24.0

        # Back up until sign change
        alt = start

        while alt * start > 0:
            alt = moonpos(tryme, self.latitude, self.longitude, 'alt')
            # Back up ~ 5 minutes
            tryme -= 0.0035
        tryme += .00419 # Forward 6 minutes

        # Forward 1 minute at a time until sign change
        while alt * start < 0:
            alt, az = moonpos(tryme, self.latitude, self.longitude, 'altaz')
            tryme += 0.00069
        tryme -= 0.00069

        # Return local time and azimuth
        return [j2local(tryme, self.utc_offset), az]

    def moon_earth(self):
        # Define the center and sizes for the orbits to be displayed
        center_x = 140
        center_y = 132
        earth_orbit = 80
        moon_orbit = 40

        # pos = [earthX, earthY, earthR, moonX, moonY, moonR]
        d = j2000(time())
        pos = celestial_bodies(d)

        # Calculate Earth's position here to check for clock collision
        feta = atan2(pos[0], pos[1])
        earth_x = center_x + earth_orbit * pos[2] * sin(feta)
        earth_y = center_y - earth_orbit * pos[2] * cos(feta)

        # Raise the Sun's position as needed to prevent clock collision
        # Save offset for icon
        offset = 0
        if earth_y > 170:
            offset = earth_y - 170
            center_y -= offset
            earth_y = 170.0

        # sun
        self.canvas.create_oval(center_x - 20, center_y - 20, center_x + 20, center_y + 20, fill='#000')
        self.canvas.create_text(center_x + 1, center_y + 27, text='sun', font=self.font12, fill='#0a0a0a', tags='sunlabel')

        # earth
        # y + 1 fixes eccentricity at this scale
        self.canvas.create_oval(center_x - earth_orbit, center_y - earth_orbit + 1, center_x + earth_orbit, center_y + earth_orbit + 1, tags='earth')
        self.canvas.create_oval(earth_x - 14, earth_y - 14, earth_x + 14, earth_y + 14, fill='#000', tags='earth')

        # moon
        feta = atan2(pos[3], pos[4])
        moon_x = earth_x + moon_orbit * pos[5] * sin(feta)
        moon_y = earth_y - moon_orbit * pos[5] * cos(feta)

        # y - 1 fixes eccentricity at this scale
        self.canvas.create_oval(earth_x - moon_orbit, earth_y - moon_orbit - 1, earth_x + moon_orbit, earth_y + moon_orbit - 1, tags='moon')
        self.canvas.create_oval(moon_x - 8, moon_y - 8, moon_x + 8, moon_y + 8, fill='#000', tags='moon')

        # earth & moon labels
        if moon_y > earth_y:
            self.canvas.create_text(earth_x + 1, earth_y - 21, text='earth', font=self.font12, fill='#0a0a0a', tags='earth')
        else:
            self.canvas.create_text(earth_x + 1, earth_y + 21, text='earth', font=self.font12, fill='#0a0a0a', tags='earth')
        self.canvas.create_text(moon_x + 1, moon_y + 15, text='moon', font=self.font12, fill='#0a0a0a', tags='moon')

        # Update icon
        if have_pil:
            image = Image.new('RGB', (280, 280), '#fff')
            draw = ImageDraw.Draw(image)

            # Keep icon centered
            center_y += offset
            earth_y += offset
            moon_y += offset

            # sun
            draw.ellipse([(center_x - 20, center_y - 20), (center_x + 20, center_y + 20)], fill='#000')

            # earth, y + 1
            draw.ellipse([(center_x - earth_orbit, center_y - earth_orbit + 1), (center_x + earth_orbit, center_y + earth_orbit + 1)], outline='#000')
            draw.ellipse([(earth_x - 14, earth_y - 14), (earth_x + 14, earth_y + 14)], fill='#000')

            # moon, y - 1
            draw.ellipse([(earth_x - moon_orbit, earth_y - moon_orbit - 1), (earth_x + moon_orbit, earth_y + moon_orbit - 1)], outline='#000')
            draw.ellipse([(moon_x - 8, moon_y - 8), (moon_x + 8, moon_y + 8)], fill='#000')

            # Crop the image.  Constant is moon radius.
            image.crop((center_x - earth_orbit - moon_orbit - 8, center_y - earth_orbit - moon_orbit - 8, center_x + earth_orbit + moon_orbit + 8, center_y + earth_orbit + moon_orbit + 8))

            # Replace icon
            icon = ImageTk.PhotoImage(image)
            self.tk.call('wm', 'iconphoto', self._w, icon)

    def drawsun(self):
        # Draw the sun at its current location along its path in the sky
        t = time()
        d = j2000(t)

#        self.pathinfo = [abs(totalmin), amplitude, width, x, height, y]

        if self.paths == 'sun' or self.paths == 'both':
            cur_angle, az = sunpos(d, self.latitude, self.longitude, 'altaz')
            cur_angle += self.pathinfo[0] # abs(totalmin)
            cur_angle /= self.pathinfo[1] # amplitude
            sun_y = (1 - cur_angle) * self.pathinfo[4] + self.pathinfo[5] # height, y
            if self.graph == 'time':
                sun_x = ((t - self.utc_offset) % SEC_IN_DAY) / SEC_IN_DAY * self.pathinfo[2] + self.pathinfo[3] # width, x
            # self.graph = 'bearing'
            else:
                sun_x = az * self.pathinfo[2] / 360.0 + self.pathinfo[3] # width, x
            self.canvas.create_oval(sun_x - 10, sun_y - 10, sun_x + 10, sun_y + 10, fill='#000', tags='sun')
            if sun_x + 10 > self.pathinfo[2]: # height
                sun_x -= self.pathinfo[2]
                self.canvas.create_oval(sun_x - 10, sun_y - 10, sun_x + 10, sun_y + 10, fill='#000', tags='sun')
            if sun_x - 10 < 0:
                sun_x += self.pathinfo[2]
                self.canvas.create_oval(sun_x - 10, sun_y - 10, sun_x + 10, sun_y + 10, fill='#000', tags='sun')

        if self.paths == 'moon' or self.paths == 'both':
            cur_angle, az = moonpos(d, self.latitude, self.longitude, 'altaz')
            cur_angle += self.pathinfo[0] # abs(totalmin)
            cur_angle /= self.pathinfo[1] # amplitude
            moon_y = (1 - cur_angle) * self.pathinfo[4] + self.pathinfo[5] # height, y
            if self.graph == 'time':
                moon_x = ((t - self.utc_offset) % SEC_IN_DAY) / SEC_IN_DAY * self.pathinfo[2] + self.pathinfo[3] # width, x
            # self.graph = 'bearing'
            else:
                moon_x = az * self.pathinfo[2] / 360.0 + self.pathinfo[3] # width, x
            self.canvas.create_oval(moon_x - 10, moon_y - 10, moon_x + 10, moon_y + 10, fill='#fff', tags='sun')
            if moon_x + 10 > self.pathinfo[2]: # height
                moon_x -= self.pathinfo[2]
                self.canvas.create_oval(moon_x - 10, moon_y - 10, moon_x + 10, moon_y + 10, fill='#fff', tags='sun')
            if moon_x - 10 < 0:
                moon_x += self.pathinfo[2]
                self.canvas.create_oval(moon_x - 10, moon_y - 10, moon_x + 10, moon_y + 10, fill='#fff', tags='sun')

    def moonphase(self):
        # Lunar constant = mean synodic month in solar days
        # Lunar epoch = the new moon 6 Jan 2000 18:14 UTC
        lunar_constant = 29.53058770576 * 60.0 * 60.0 * 24.0
        lunar_epoch = 947182440.0

        # Calculate the age of the moon, 0 = new, 1 = full
        age = ((time() - lunar_epoch) % lunar_constant) / lunar_constant

        # Convert age to phase, 0/1 = full, 0.5 = new
        phase = age - 0.5
        if phase < 0:
            phase += 1

        moon_center_x = 256.0
        moon_center_y = 24.0
        moon_radius = 20.0

        # Draw a black circle for the new moon
        self.canvas.create_oval(moon_center_x - moon_radius, moon_center_y - moon_radius, moon_center_x + moon_radius, moon_center_y + moon_radius, fill='#000')

        # For each row, draw white lines for the illuminated section
        # From https://www.codeproject.com/Articles/100174/Calculate-and-Draw-Moon-Phase
        for ypos in range(0, int(moon_radius)):
            xpos = sqrt(moon_radius * moon_radius - ypos * ypos)
            # Draw filled in circle
#            self.canvas.create_line(moon_center_x - xpos, moon_center_y + ypos, moon_center_x + xpos, moon_center_y + ypos)
#            self.canvas.create_line(moon_center_x - xpos, moon_center_y - ypos, moon_center_x + xpos, moon_center_y - ypos)
            rpos = 2 * xpos
            if phase < 0.5:
                xpos1 = xpos * -1
                xpos2 = rpos - 2 * phase * rpos - xpos
            else:
                xpos1 = xpos
                xpos2 = xpos - 2 * phase * rpos + rpos
            # Draw lit part of the Moon
            self.canvas.create_line(moon_center_x + xpos1, moon_center_y - ypos, moon_center_x + xpos2, moon_center_y - ypos, fill='#fff')
            self.canvas.create_line(moon_center_x + xpos1, moon_center_y + ypos, moon_center_x + xpos2, moon_center_y + ypos, fill='#fff')

        # Draw a moon outline
        self.canvas.create_oval(moon_center_x - moon_radius, moon_center_y - moon_radius, moon_center_x + moon_radius, moon_center_y + moon_radius)

    def getdst(self, time):
        # Check for DST at time
        is_dst = daylight and localtime(time).tm_isdst > 0
        return altzone if is_dst else timezone

    def timer(self):
        # Update clock
        self.canvas.itemconfig(self.clocklabel, text=strftime('%H:%M'))

        # Countdown to top of next minute
        nextmin = 60 - (int(strftime('%S')) % 60)
        self.after(nextmin * 1000, self.timer)

        # Update orbit, sun path, sunrise, and sunset once per day
        curday = strftime('%w')
        if curday != self.oldday:
            # Check DST at noon
            self.utc_offset = self.getdst(time() + 43200)

            # Check for DST change today
            self.dst_today = self.getdst(time()) != self.utc_offset

            self.oldday = curday
            self.counter = 0
            self.canvas.delete('all')
            self.clocklabel = self.canvas.create_text(140, 270, text=strftime('%H:%M'), font=self.clockfont, fill='#000')
            self.sun_paths()
            self.moon_earth()
            self.moonphase()

        self.counter += 1

        # Update sun position every 4 minutes
        if self.counter % 4 == 0:
            self.canvas.delete('sun')
            self.drawsun()

        # Update sun-earth-moon, due to the moon, every 136 minutes
        if self.counter % 136 == 0:
            self.canvas.delete('earth', 'moon', 'sunlabel')
            self.moon_earth()

        # Update moon phase every 532 minutes
        if self.counter > 531:
            self.counter = 0
            self.moonphase()

    def showGUI(self):
        # clock label
        self.clocklabel = self.canvas.create_text(140, 270, font=self.clockfont, fill='#000')

        # This works on most days...
        self.utc_offset = self.getdst(time())

        # Check 24 hours in the past and 24 hours in the future for DST change
        self.dst_today = self.getdst(time() - 86400) != self.utc_offset or self.getdst(time() + 86400) != self.utc_offset

        self.oldday = strftime('%w')
        self.sun_paths()
        self.moon_earth()
        self.moonphase()
        self.timer()

if __name__ == '__main__':
    app = Gazer()
    app.mainloop()